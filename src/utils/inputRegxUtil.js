
export function isLetter(e) {
    let char = String.fromCharCode(e.keyCode); // รับค่าตัวอักษร
    if (/^[A-Za-z0-9-/ ]+$/.test(char)) return true;
    // ตั้งค่าเงื่อนไข
    else e.preventDefault(); // ถ้าเกิดว่า input ค่าที่รับเข้ามาไม่ตรงกับเงื่อนไขจะไม่ทำงาน
}
export function isLetterNum(e) {
    let char = String.fromCharCode(e.keyCode); // รับค่าตัวอักษร
    if (/^[0-9.]+$/.test(char)) return true;
    // ตั้งค่าเงื่อนไข
    else e.preventDefault(); // ถ้าเกิดว่า input ค่าที่รับเข้ามาไม่ตรงกับเงื่อนไขจะไม่ทำงาน
}
export function isLetterCha(e) {
    let char = String.fromCharCode(e.keyCode); // รับค่าตัวอักษร
    if (/^[A-Za-z]+$/.test(char)) return true;
    // ตั้งค่าเงื่อนไข
    else e.preventDefault(); // ถ้าเกิดว่า input ค่าที่รับเข้ามาไม่ตรงกับเงื่อนไขจะไม่ทำงาน
}

