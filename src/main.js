import '@babel/polyfill'
import 'mutationobserver-shim'
import Vue from 'vue'
import './plugins/bootstrap-vue'
import App from './App.vue'
import router from './router'
import api from "./service";
Vue.config.productionTip = false
Vue.prototype.$api = api;
new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
