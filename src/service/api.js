import axios from "axios";

const API = axios.create({
    baseURL: 'http://localhost:3000',
    headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
    }
})
const handleResponseData = (resp) => resp.data;

export const request = async (type, path, data, responseType) => {
    let error, result;
    var resp;
    switch (type) {
        case "GET":
            resp = await API.get(path, { params: data });
            break;
        case "POST":
            resp = await API.post(path, data, { responseType: responseType })
                .then()
                .catch();
            break;
        case "PUT":
            resp = await API.put(path, data);
            break;
        case "DELETE":
            resp = await API.delept(path, { path, data });
            break;
        default:
            resp = await API.get(path, { params: data });
            break;
    }
   
    if (!resp) {
        error = resp;
        result = null;
    }
    result = handleResponseData(resp);
    return {
        error,
        result,
    };

}
export default API;

