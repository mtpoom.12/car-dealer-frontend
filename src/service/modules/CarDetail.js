import { request } from '../api'
export const SERVICE = 'http://localhost:3000'

export const getCars = async () => {
    const { error, result } = await request('POST', SERVICE + '/cars')
    return { error, result }
}

export const searchCars = async (str) => {
    const { error, result } = await request('POST', SERVICE + '/cars?search='+str)
    return { error, result }
}

export const addCars = async (item) => {
    const { error, result } = await request('POST', SERVICE + '/addCars', item)
    return { error, result }
}


export const editCars = async (item) => {
    const { error, result } = await request('POST', SERVICE + '/updateCars/'+item.id, item)
    return { error, result }
}

export const deleteCars = async (id) => {
    const { error, result } = await request('POST', SERVICE + '/deleteCar/'+id)
    return { error, result }
}



